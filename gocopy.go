package main

import (
	"flag"
	"io"
	"log"
	"os"
	"github.com/cheggaaa/pb/v3"
	"math"
)

var (
	fromPath string
	toPath   string
	offset   int64
	limit    int64
)

func init() {
	flag.StringVar(&fromPath, "from", "loh", "path to a file to read from")
	flag.StringVar(&toPath, "to", "", "path to a file to write to")
	flag.Int64Var(&offset, "offset", 0, "bytes offset in the \"from\" file")
	flag.Int64Var(&limit, "limit", 0, "bytes limit to copy from the \"from\" file")
}

func main() {
	flag.Parse()

	// Open the source file.
	fromFile, err := os.Open(fromPath)
	if err != nil {
		log.Fatalf("Cannot open the \"from\" file. Please make sure that the file is valid. Error: %v", err)
	}
	defer fromFile.Close()

	// Open with reset or create the destination file.
	toFile, err := os.Create(toPath)
	if err != nil {
		log.Fatalf("Cannot create/open the \"to\" file. Error %v", err)
	}
	defer toFile.Close()

	// Move the Read/Write pointer of the "fromFile" to provided offset.
	_, err = fromFile.Seek(offset, io.SeekStart)
	if err != nil {
		log.Fatalf("Failed to move pointer in the \"from\" file to the provided offset. Error: %v", err)
	}

	// Create progress bar depending on file size, provided limit and offset.
	progressBar := makeProgressBar(fromFile, limit, offset)
	barReader := progressBar.NewProxyReader(fromFile)
	// Copy "limit" of bytes from the "fromFile" to the "toFile"
	_, err = io.CopyN(toFile, barReader, limit)
	if err != nil && err != io.EOF {
		log.Fatalf("Failed to copy data. Error: %v", err)
	}

	progressBar.Finish()
}

func makeProgressBar(fromFile *os.File, limit, offset int64) *pb.ProgressBar {
	fromFileInfo, err := fromFile.Stat()
	if err != nil {
		log.Fatalf("Failed to get the \"from\" file length. Error: %v", err)
	}

	progressLimit := math.Min(float64(limit), float64(fromFileInfo.Size()))

	return pb.Full.Start64(int64(progressLimit) - offset)
}
