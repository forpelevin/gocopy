package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strconv"
	"testing"
)

const appName = "gocopy"

func TestGoCopy(t *testing.T) {
	tests := []struct {
		description           string
		fromFilePath          string
		fromFileContent       string
		fromFileArg           string
		limitArg              int64
		offsetArg             int64
		commandErrorExpected  bool
		expectedWrittenResult string
	}{
		{
			"case when we copy all bytes",
			"path",
			"test",
			"path",
			10,
			0,
			false,
			"test",
		},
		{
			"case with offset",
			"path",
			"test",
			"path",
			10,
			2,
			false,
			"st",
		},
		{
			"case when we copy with offset and limit",
			"path",
			"testing",
			"path",
			2,
			3,
			false,
			"ti",
		},
		{
			"case when we write nothing",
			"path",
			"testing",
			"path",
			0,
			1,
			false,
			"",
		},
		{
			"case when we try to copy from non-existed file",
			"path",
			"testing",
			"nonexisted",
			10,
			0,
			true,
			"",
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			dir, err := os.Getwd()
			if err != nil {
				t.Fatal(err)
			}
			cliName := fmt.Sprintf("%s.go", appName)
			appPath := path.Join(dir, cliName)

			// Create a fake file.
			fromFile, err := os.Create(tc.fromFilePath)
			if err != nil {
				t.Fatalf("Failed to create a fake file. Error: %v", err)
			}
			// Delete the file after the test case
			defer fromFile.Close()
			defer os.Remove(fromFile.Name())

			// Fill the fake file with the test case content.
			_, err = fromFile.Write([]byte(tc.fromFileContent))
			if err != nil {
				t.Fatalf("Failed to fill out the fake file with content. Error: %v", err)
			}

			destinationName := "fake-to-file"

			args := []string{
				"run", appPath,
				"-from", tc.fromFileArg,
				"-to", destinationName,
				"-limit", strconv.Itoa(int(tc.limitArg)),
				"-offset", strconv.Itoa(int(tc.offsetArg)),
			}
			cmd := exec.Command("go", args...)
			cmd.Env = append(os.Environ(), "GOPATH=/tmp/go")
			output, err := cmd.CombinedOutput()
			if err != nil {
				if tc.commandErrorExpected == true {
					return
				}
				t.Fatalf("Failed with output: %s, and error: %v", output, err)
			}

			destinationFile, err := os.Open(destinationName)
			if err != nil {
				t.Fatalf("Failed to open the destination file after the command execution. Error: %v", err)
			}
			defer destinationFile.Close()
			defer os.Remove(destinationName)

			copyResult, err := ioutil.ReadAll(destinationFile)
			if err != nil {
				t.Fatalf("Failed to read the destination file after the command execution. Error: %v", err)
			}

			if tc.expectedWrittenResult != string(copyResult) {
				t.Fatalf(
					"Expected that the copy result will be %s but got %s",
					tc.expectedWrittenResult,
					string(copyResult),
				)
			}
		})
	}
}
