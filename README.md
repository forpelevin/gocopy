# Go Copy Utility
The CLI app that is "dd" utility alternative written on pure Golang.
## Getting Started
```
go get -u github.com/forPelevin/gocopy
```
## Prerequisites
For the successful using you should have:
```
go >= 1.12
```
## Running the tests
It's a good practice to run the tests before using the app to make sure everything is OK.
```
cd $GOPATH/src/github.com/forPelevin/gocopy 
make test
```
## Build the app
```
cd $GOPATH/src/github.com/forPelevin/gocopy
make build
```
## Sample of using
```
// The command will copy 20 bytes (with 10 bytes offset) 
// from the "source-file" to the "dest-file"
gocopy --from source-file --to dest-file --offset 10 --limit 20
```
## License
This project is licensed under the MIT License.